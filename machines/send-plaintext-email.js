module.exports = {

  friendlyName: 'Send Plaintext Email',

  description: 'Send a simple plaintext email.',

  extendedDescription: 'Send an email using JangoSMTP\'s service.  You can test the Jango API method here: http://api.jangomail.com/api.asmx?op=SendTransactionalEmail',

  inputs: {
    username: {
      example: '',
      description: 'Your JangoSMTP username',
      required: true
    },
    password: {
      example: '',
      description: 'Your JangoSMTP password',
      required: true
    },
    toEmail: {
      example: 'jane@example.com',
      description: 'Email address of the primary recipient.',
      required: true
    },
    subject: {
      required: true,
      description: 'Subject line for the email.',
      example: 'Welcome, Jane!'
    },
    textMessage: {
      required: true,
      description: 'The plaintext body of the email.',
      example: 'Jane,\nThanks for joining our community.  If you have any questions, please don\'t hesitate to send them our way.  Feel free to reply to this email directly.\n\nSincerely,\nThe Management'
    },
    fromEmail: {
      required: true,
      description: 'Email address of the sender.',
      example: 'harold@example.enterprise'
    },
    fromName: {
      required: true,
      description: 'Full name of the sender.',
      example: 'Harold Greaseworthy'
    },
    options: {
      description: 'Options string to set additional options.',
      extendedDescription: 'Must be comma separated string of option=value pairs with these options: TransactionalGroupID, ReplyTo, CC, BCC, CharacterSet, Encoding, Priority, UseSystemMAILFROM, Receipt, Wrapping, ClickTrack, OpenTrack, NoClickTrackText, Attachment1, Attachment2, Attachment3, Attachment4, Attachment5, SkipUnsubCheck. For example, an acceptable value for this parameter would be "OpenTrack=True,ClickTrack=True,ReplyTo=john@hotmail.com". TransactionalGroupID, if specified, must be the ID number assigned to a Transactional Group that has been created under My Options of the JangoMail account. UseSystemMAILFROM can be "True" or "False". ReplyTo must be a valid e-mail address. CC must be a valid e-mail address. BCC must be a valid e-mail address. Encoding must be "7 bit", "Base 64", or "Quoted Printable". CharacterSet must be one of the official JangoMail character sets. Receipt must be "True" or "False". Priority must be "Low", "Medium", or "High". Wrapping must be an integer >= 0. ClickTrack must be "True" or "False". OpenTrack must be "True" or "False". NoClickTrackText must be "True" or "False". Attachment1 through 5 must be valid file names that have already been uploaded to your account. SkipUnsubCheck, which defaults to "False", can be set to "True" to avoid checking recipients against your unsubscribe list.',
      example: 'OpenTrack=True,ClickTrack=True,ReplyTo=john@hotmail.com',
      whereToGet: {
        url: 'http://api.jangomail.com/help/html/655d20ca-2164-c483-fb21-d3d0ee049155.htm'
      }
    }
  },

  defaultExit: 'success',

  exits: {

    error: {
      description: 'Unexpected error occurred.'
    },

    wrongOrNoUsernamePassword: {
      description: 'Missing or incorrect username/password.'
    },

    success: {
      description: 'If successful, returns an object containing 3 key/value pairs: code=0, result=SUCCESS, transactionID=the numeric transactional email ID from Jango.'
    }

  },

  fn: function(inputs, exits) {
    var util = require('util');
    var Http = require('machinepack-http');

    Http.sendHttpRequest({
      url: 'https://api.jangomail.com/api.asmx/SendTransactionalEmail',
      method: 'get',
      params: {
        Username: inputs.username,
        Password: inputs.password,
        FromEmail: inputs.fromEmail,
        FromName: inputs.fromName,
        ToEmailAddress: inputs.toEmail,
        Subject: inputs.subject,
        MessagePlain: inputs.textMessage || '',
        MessageHTML: '',
        Options: inputs.options || ''
      }/*,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }*/
    }).exec({
      // OK.
      success: function(result) {

        // Unfortunately, Jango only returns XML and it difficult to parse
        var xml = result.body;

        if (xml.indexOf('"<?xml') != 0) {
          return exits.error('There was an error parsing the XML response.');
        }

        // remove the backslashes around the quotes
        xml = xml.replace(/\\"/g, '\"');

        // remove the header
        xml = xml.replace('<?xml version="1.0" encoding="utf-8"?>', '');

        //remove CRLFs
        var pattern = new RegExp("\\\\r\\\\n", "g");
        xml = xml.replace(pattern, '');

        // remove the <string>
        xml = xml.replace("<string xmlns=\"http://api.jangomail.com/\">", '');
        // remove the </string>
        xml = xml.replace("</string>", "");

        // remove remaining double quote characters
        xml = xml.replace(/"/g, '');

        // The remaining text is what we want.  Split it on the new line characters
        var parts = xml.split("\\n");
        if (util.isArray(parts) && parts.length === 3) {
          var resultObj = {
            code: parts[0],
            result: parts[1],
            transactionId: parts[2]
          };
          return exits.success(resultObj);
        }
        return exits.error('There was a problem parsing the XML response.');
      },
      error: function(err) {
        if (err) return exits.error(err);
      },
      // 404 status code returned from server
      notFound: function(result) {
        if (err) return exits.error(err);
      },
      // 400 status code returned from server
      badRequest: function(result) {
        return exits.error('Something was wrong with your HTTP request (' + result.status + ').');
      },
      // 403 status code returned from server
      forbidden: function(result) {
        return exits.wrongOrNoUsernamePassword('Invalid or unprovided username/password (403).');
      },
      // 401 status code returned from server
      unauthorized: function(result) {
        return exits.wrongOrNoUsernamePassword('Invalid or unprovided username/password (401).');
      },
      // 5xx status code returned from server (this usually means something went wrong on the other end)
      serverError: function(result) {
        result.machineMessage = 'Something went wrong on the other end (' + result.status + ').';
        return exits.error(result);
      },
      // Unexpected connection error: could not send or receive HTTP request.
      requestFailed: function() {
        return exits.error('Could not send or receive the HTTP request.');
      }
    })

    /*mailgun.messages().sendMime(dataToSend, function (err, body) {
     if (err) return exits.error(err);
     return exits.success();
     });*/
  }

};
